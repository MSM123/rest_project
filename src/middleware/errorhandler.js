function errorHandler(err, req, res, next) {
  console.log('here inside the error logger in middle ware');
  res.status(404).send(err.message);
  console.log("after sending");
  next(err.message);
}
module.exports = errorHandler;
