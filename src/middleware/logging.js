const winston = require('winston');
const expressWinston = require('express-winston');
    const errLogger = expressWinston.errorLogger({
    transports: [
        new winston.transports.File({
            filename: "generatedErrorLogs.log",
            level: "info"
        })
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
});
module.exports = errLogger;