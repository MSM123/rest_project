const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'rest_sql',
  password: 'mausam20189',
  port: 5432
});
//queries for getting director name of movie by movie id
async function readMoviesByDirector(params) {
  return await pool.query(
    `SELECT Director FROM movies WHERE Id = ${params.Id}`
  );
}
//queries for getting all movies by a director
async function getAllMoviesByDirector(params) {
  const x=Object.keys(params)
  const y=Object.values(params)
  return await pool.query(
    `SELECT * FROM movies WHERE ${x} = '${y}';`
  );
}

//queries for deleting movies details by movie id
async function deleteMovies(params) {
  const deletedMovies = await pool.query(
    `DELETE FROM movies WHERE Id = ${params.Id} RETURNING *;`
  );
  console.log(deletedMovies);
  return deletedMovies;
}
//queries for updating the movies details by movie id
async function updateMovies(body) {
  const updatedMovies = await pool.query(
    `UPDATE movies SET Director = '${body.Director}' WHERE Id = ${body.Id} RETURNING *;`
  );
  console.log(updatedMovies);
  return updatedMovies;
}
//creating new movies details
async function createMovies(body) {
  const createdMovies = await pool.query(
    `INSERT INTO movies (Id,Title,Year,Genre,Director,Imdb_rating,runTime_mnt) VALUES (${body.Id},'${body.Title}',${body.Year}, '${body.Genre}','${body.Director}',${body.Imdb_rating},${body.runTime_mnt}) RETURNING *;`
  );
  console.log(createdMovies);
  return createdMovies;
}
//exporting functions
module.exports = {
  readMoviesByDirector,
  getAllMoviesByDirector,
  deleteMovies,
  updateMovies,
  createMovies
};
