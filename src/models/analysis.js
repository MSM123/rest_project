const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'rest_sql',
  password: 'mausam20189',
  port: 5432
});
//queries for getting writer name by movie id
async function getMoviesWriterById(params) {
  return await pool.query(
    `SELECT Writer FROM analysis WHERE Id = ${params.Id}`
  );
}
//queries for deleting the movie id with all details
async function deleteMoviesId(params) {
  const deletedId = await pool.query(
    `DELETE FROM analysis WHERE Id = ${params.Id} RETURNING *;`
  );
  console.log(deletedId);
  return deletedId;
}
//queries for updating the movies analysis
async function updateAnalysis(body) {
const updatedBoxOffColl = await pool.query(
`UPDATE analysis SET BO_collection_cr = ${body.BO_collection_cr} WHERE Id = ${body.Id} RETURNING *;`
  );
  // console.log(updatedBoxOffColl);
  return updatedBoxOffColl.rows;
}
//queries for creating new movie analysis 
async function createMoviesId(body) {
  const createdMoviesId = await pool.query(
`INSERT INTO analysis (Id,Writer,BO_collection_cr,BO_Analysis) VALUES (${body.Id},'${body.Writer}',${body.BO_collection_cr}, '${body.BO_Analysis}') RETURNING *;`
  );
  console.log(createdMoviesId);
  return createdMoviesId;
}
//export the functions
module.exports = {
  getMoviesWriterById,
  deleteMoviesId,
  updateAnalysis,
  createMoviesId
}