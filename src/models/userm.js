const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'rest_sql',
  password: 'mausam20189',
  port: 5432
});
//query for inserting users details in "users" table
async function insertValuesInUsers(body,hashPass) {
var createdUsers = await pool.query(
`INSERT INTO users (user_name,email,password) VALUES ('${body.user_name}','${body.email}','${hashPass}') RETURNING *;`
);
console.log(createdUsers);
return createdUsers;
}
//query for checking valid email
async function findValidEmail(email) {
    return await pool.query(
      'SELECT * FROM users WHERE email=$1', [email]
      );
}
//exporting module
module.exports = {
  insertValuesInUsers,
  findValidEmail
};
  