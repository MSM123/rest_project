var express = require('express');
var router = express.Router();
const analysisr = require('/home/mausam/Desktop/rest/src/models/analysis.js');
module.exports = router;
//get
router.get('/:Id', async function(req, res) {
    const res1 = req.params;
    const res2 = await analysisr.getMoviesWriterById(res1);
    res.send(res2);
  });
//delete
router.delete('/:Id', async function(req, res) {
    const res1 = req.params;
    const res2 = await analysisr.deleteMoviesId(res1);
    res.send(res2);
});
//put
router.put('/BO_collection_cr', async function(req, res) {
    const res1 = req.body;
    const res2 = await analysisr.updateAnalysis(req.body);
    console.log(res2);
    res.send(res2);
});
//post
router.post('/', async function(req, res) {
    const res1 = req.body;
    const res2 = await analysisr.createMoviesId(res1);
    res.send(res2);
});
