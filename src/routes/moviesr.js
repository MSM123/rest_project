var express = require('express');
var validator = require('/home/mausam/Desktop/rest/src/utils/validation.js');
var router = express.Router();
const moviesr = require('/home/mausam/Desktop/rest/src/models/movies.js');
module.exports = router;
//get
router.get('/:Id', async function(req, res) {
  const res1 = req.params;
  const res3 = validator.readMoviesDirector(res1);
  const res2 = await moviesr.readMoviesByDirector(res1);
  res.send(res2.rows);
});

/*using query parameter
router.get('/details', async function(req, res) {
  const res1 = req.query;
  const res3 = validator.readMoviesTitle(res1);
  const res2 = await moviesr.getAllMoviesByDirector(res1);
  res.send(res2.rows);
});
*/
//delete
router.delete('/:Id', async function(req, res) {
  const res1 = req.params;
  const res3 = validator.deleteAllDetail(res1);
  const res2 = await moviesr.deleteMovies(res1);
  res.send(res2.rows);
});
//put
router.put('/:Id', async function(req, res) {
  const res1 = req.body;
  const res3 = validator.updateAllDetail(res1);
  const res2 = await moviesr.updateMovies(res1);
  res.send(res2.rows);
});
//post
router.post('/update', async function(req, res) {
  const res1 = req.body;
  const res3 = validator.createAllDetail(res1);
  const res2 = await moviesr.createMovies(res1);
  res.send(res2.rows);
});
