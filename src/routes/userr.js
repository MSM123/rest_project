const express = require('express');
const router = express.Router();
module.exports = router;
const usersController = require('/home/mausam/Desktop/rest/src/controller/users.js');
//routes for register user
router.post('/register',usersController.registerUser);
//routes for login user
router.post('/login', usersController.loginUser);