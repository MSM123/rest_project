const Joi = require('joi');

function readMoviesDirector(Id){
const getSchema = {
Id : Joi.number().min(1).required()
};
return Joi.validate(Id,getSchema);
}
/*
function readMoviesTitle(params){
    const getSchema = Joi.object().keys({
    values1 : Joi.string().min(5).required(),
    values2 : Joi.number().min(1).required(),
    });
    return Joi.validate(params,getSchema);
    }
*/
function deleteAllDetail(Id){
        const getSchema = {
        Id : Joi.number().min(1).required()
        };
        return Joi.validate(Id,getSchema);
        }
function updateAllDetail(Id){
            const getSchema = {
            Id : Joi.number().min(1).required()
            };
            return Joi.validate(Id,getSchema);
}
function createAllDetail(obj){
    const getSchema = Joi.object().keys({
    Id : Joi.number().min(1).required(),
    Title : Joi.string().min(1).required(),
    Year : Joi.number().min(4).required(),
    Genre : Joi.string().min(3).required(),
    Director : Joi.string().min(5).required(),
    Imdb_rating : Joi.number().min(1).required(),
    runTime_mnt : Joi.number().min(1).required()
    });
    return Joi.validate(obj,getSchema);
}
module.exports = {
    readMoviesDirector,
   // readMoviesTitle,
    deleteAllDetail,
    updateAllDetail,
    createAllDetail
};
