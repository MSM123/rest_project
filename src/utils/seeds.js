const { Pool } = require('pg');
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'rest_sql',
  password: 'mausam20189',
  port: 5432,
});
//create table for movies csv data
function createTableMovies() {
return pool.query(
'CREATE TABLE movies(Id INTEGER,Title VARCHAR(60),Year INTEGER,Genre VARCHAR(60),Director VARCHAR(60),Imdb_rating FLOAT,runTime_mnt INTEGER)'
);
}
//create table for analysis csv data
function createTableAnalysis() {
return pool.query(
'CREATE TABLE analysis(Id INTEGER,Writer VARCHAR(60),BO_collection_cr INTEGER,BO_Analysis VARCHAR(60))'
);
}
//create table for users
function createUsers() {
  return pool.query(
    'CREATE TABLE IF NOT EXISTS users(name VARCHAR,email VARCHAR PRIMARY KEY,password VARCHAR)'
  );
}

//insert data into movies table
function insertDataToMoviesTable() {
return pool.query(
`COPY movies FROM '/home/mausam/Desktop/rest/src/data/movies.csv' with delimiter ',' csv header`
);
}
//insert data into analysis table
function insertDataToAnalysisTable() {
return pool.query(
`COPY analysis FROM '/home/mausam/Desktop/rest/src/data/analysis.csv' with delimiter ',' csv header`
);
}