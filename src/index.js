const express = require('express');
const app = express();
app.use(express.json());
//path of logger
const logger = require('/home/mausam/Desktop/rest/src/middleware/logging.js');
//path of errorhandler
const errorhandler = require('/home/mausam/Desktop/rest/src/middleware/errorhandler.js');
//path of movies api
const moviesRoutes = require('/home/mausam/Desktop/rest/src/routes/moviesr.js');
//path of analysis api
const analysisRoutes = require('/home/mausam/Desktop/rest/src/routes/analysisr.js');
//path of user api
const userRoutes = require('/home/mausam/Desktop/rest/src/routes/userr.js');
//port settings
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
//calling api to connnect server
app.use('/movies', moviesRoutes);
app.use('/', analysisRoutes);
app.use('/user',userRoutes);
// error messege
app.use((req, res, next) => {
  err = new Error('error generated');
  next(err);
});
app.use(errorhandler);
app.use(logger);
