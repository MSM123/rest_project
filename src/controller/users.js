const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const models = require('/home/mausam/Desktop/rest/src/models/userm.js');
// user registration
const registerUser = async function(req, res) {
    const res1 = req.body;
    const salt = await bcrypt.genSalt(7);
   const hashedPassword = await bcrypt.hash(res1.password, salt);
    const res2 = await models.insertValuesInUsers(res1,hashedPassword);
    res.send(res2.rows);
}
// user login
const loginUser = async function (request, res, next) {
    const validUser = await models.findValidEmail(request.body.email);
    if (validUser.rowCount === 0) {
      res.status(400);
      next({ message: 'Email incorrect', statusCode: 400 });
    } else {
        const token = jwt.sign(
          { user_name: validUser.rows[0].role },
          process.env.TOKEN_SECRET || "SOmeDDstbgyekiGFe76");
        res.header('auth-token', token).send('Login Success - ' + token);
      }
    }

    //exporting functions
  module.exports = {
      registerUser,
      loginUser
  };