Build the backend APIs for a web app.

The application must have at least 2 entities with a relationship between them. eg. books and authors. Sample site - https://www.goodreads.com/

Directory structure

src:

config/
middleware/
models:

anime.js
genre.js
routes:

anime.js
genre.js
utils:

validation.js
logging.js
config.js
db.js
index.js
package.json
Setup
- Install Eslint (Airbnb style)
- Install Nodemon
- Write NPM scripts to run Eslint and test
- Setup a postgres database
- Create a seedData script to create tables and add sample data
Database models
- Create a database module for each entity
- Protect sql injection by sanitizing data
- Error handling in database module for all database calls
Build the RESTful API using ExpressJS
- Install express and use it to run the server
- Install nodemon and use it as npm script
- Create RESTful APIs using express
- Appropriate response codes from the APIs
- Use Express router to make it modular
Logging, Error handling, Validation
- Setup logging using a npm module winston
- Form Validation using npm module joi
- Error handling using express middleware
- [Optional] Use Sequelize for Database-ORM.
Authentication
- Validation and encryption.
- Register and authenticate a user.
- Create and use JWT tokens.
- Allow only authorized user to access resources